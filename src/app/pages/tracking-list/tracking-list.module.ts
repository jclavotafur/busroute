import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TrackingListPageRoutingModule } from './tracking-list-routing.module';

import { TrackingListPage } from './tracking-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TrackingListPageRoutingModule
  ],
  declarations: [TrackingListPage]
})
export class TrackingListPageModule {}
