import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TrackingListPage } from './tracking-list.page';

describe('TrackingListPage', () => {
  let component: TrackingListPage;
  let fixture: ComponentFixture<TrackingListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackingListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TrackingListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
