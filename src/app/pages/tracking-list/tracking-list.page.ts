import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

//MODELS
import { TrackingModel } from '../../models/tracking.model'

//SERVICE
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-tracking-list',
  templateUrl: './tracking-list.page.html',
  styleUrls: ['./tracking-list.page.scss'],
})
export class TrackingListPage implements OnInit {

  public tracking = new TrackingModel();
  public trackings: Array<TrackingModel> = [];

  constructor(private alertController: AlertController,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    fetch('./assets/data/tracking.json').then(res => res.json())
      .then(json => {
        //this.trackings = json;
        console.log('json', json);

        this.trackings = json.map(function callback(value) {

          let tracking = new TrackingModel();
          tracking.id = value.id;
          tracking.number = value.number;
          tracking.latitude = value.latitude;
          tracking.longitude = value.longitude;
          tracking.date = value.date;

          return tracking;

        });

      });
  }

    ionViewDidEnter() {
    //IMPORTANT = this line below should be at any page to Validate Session
    !this.authenticationService.isAuthenticated() ? this.authenticationService.closeSession() : null;
  }

  async askForDelete(tracking_id: string, number: string) {

    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Deleted number <strong>' + number + '?</strong>!!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          // handler: (blah) => {
          //   console.log('Confirm Cancel: blah');
          // }
        }, {
          text: 'Okay',
          handler: () => {
            this.delete(tracking_id);
          }
        }
      ]
    });

    await alert.present();
  }

  delete(tracking_id: string) {
    console.log('Deleting id: ' + tracking_id);
    // this.CRUDEmployee(EmployeeListPage.CRUD_APAGAR, null);
  }

}
