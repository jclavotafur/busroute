import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
//MODELS
import { TrackingModel } from '../../models/tracking.model'

//SERVICES
import { ValidatorService } from '../../services/helpers/validator.service'
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-tracking',
  templateUrl: './tracking.page.html',
  styleUrls: ['./tracking.page.scss'],
})
export class TrackingPage implements OnInit {

  public tracking = new TrackingModel();
  public trackings: Array<TrackingModel> = [];

  constructor(private geolocation: Geolocation,
    private validatorService: ValidatorService,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {

    this.tracking.id = this.activatedRoute.snapshot.paramMap.get('id');

    console.log('this.tracking.id', this.tracking.id);

    if (this.tracking.id) {

    }
    else {
      this.getCoordenates();
    }

  }

  // ionViewDidEnter() {
  //   //IMPORTANT = this line below should be at any page to Validate Session
  //   !this.authenticationService.isAuthenticated() ? this.authenticationService.closeSession() : null;
  // }


  save(formTracking: NgForm) {
    this.tracking.date = this.validatorService.getCurrentTime();
    console.log('Tracking saved');
    this.trackings.push(this.tracking);
  }

  getCoordenates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.tracking.latitude = String(resp.coords.latitude).substr(0, 8);
      this.tracking.longitude = String(resp.coords.longitude).substr(0, 8);;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }



}
