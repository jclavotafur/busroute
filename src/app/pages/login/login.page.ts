import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


//Models
import { UserModel } from '../../models/user.model';

//SERVICE
import { AlertService } from '../../services/helpers/alert.service';
import { ProgressIndicatorService } from '../../services/helpers/progress-indicator.service';
import { UserService } from '../../services/user.service';
import { ValidatorService } from '../../services/helpers/validator.service';
import { AuthenticationService } from '../../services/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  static TITLE_LOGIN: string = 'Login';
  static TITLE_REGISTER: string = 'Register';

  public user = new UserModel();
  public registerFlag: boolean = false;
  public title: string = 'a';

  constructor(private alertService: AlertService,
    private progressIndicatorService: ProgressIndicatorService,
    private router: Router,
    private userService: UserService,
    private validator: ValidatorService,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.setTitle(LoginPage.TITLE_LOGIN);
  }

  ionViewDidEnter()
  {
    //This code is to check authentication
    this.authenticationService.setLoggedIn(false);
  }

  activateRegister() {
    this.user = new UserModel();
    this.registerFlag = true;
    this.setTitle(LoginPage.TITLE_REGISTER);
  }

  cancel() {
    this.registerFlag = false;
    this.setTitle(LoginPage.TITLE_LOGIN);
  }

  async register() {

    if(!this.isRegisterValidated(this.user.name, this.user.email,this.user.password,this.user.c_password))
      return;
    
    const loading = await this.progressIndicatorService.createLoading();
    loading.present();

    this.userService.register(this.user).subscribe(resultado => {

      this.alertService.presentToast(resultado.message);
      loading.dismiss();

      resultado.status ? this.registerFlag = false : this.registerFlag = true;     

    }, error => {
      this.alertService.presentToast('error' + error);
      loading.dismiss();
    });

  }

  async login() {

    if(!this.isLoginValidated(this.user.email,this.user.password))
      return;
    
    const loading = await this.progressIndicatorService.createLoading();
    loading.present();

    this.userService.login(this.user).subscribe(response => {

      this.alertService.presentToast(response.message);
      loading.dismiss();

      if(response.status)
      {
        this.authenticationService.setLoggedIn(true);
        this.authenticationService.setUser(response.result);
        this.router.navigate(['/tracking-list']);
      }
      
    }, error => {
      this.alertService.presentToast(error);
      loading.dismiss();
    });
  }

  isLoginValidated(email:string, password: string): boolean
  {
   if (!this.validator.isEmail(email))
   {
      this.alertService.presentToast(this.validator.isEmailMessage());
      return false;
   }
    
   if (!this.validator.stringHasData(password))
   {
      this.alertService.presentToast(this.validator.stringHasDataMessage());
      return false;
   }
   
    return true;
  }

  isRegisterValidated(name:string, email:string, password: string, repassword: string): boolean
  {
    if (!this.validator.stringHasData(name))
    {
       this.alertService.presentToast(this.validator.stringHasDataMessage());
       return false;
    }
    
    if(!this.isLoginValidated(email, password))
      return false;

    if(password != repassword)
    {
      this.alertService.presentToast("passwords are not the same");
      return false
    }
      
    return true;
  }


  setTitle(title: string)
  {
    this.title = title;
  }
}