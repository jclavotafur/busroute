import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { By } from '@angular/platform-browser';

import { LoginPage } from './login.page';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from '../../app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { DebugElement } from '@angular/core';


describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;
  let debugElement: DebugElement;
  let htmlElement: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginPage],
      imports: [IonicModule.forRoot(),FormsModule, HttpClientModule,AppRoutingModule],
      providers: [ ]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
  });


  afterEach(() => {
    fixture.destroy();
    component = null;
  });

  it('Should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should initialize with title "Login"', () => {
    
    fixture.detectChanges();

    debugElement = fixture.debugElement.query(By.css('ion-title'));
    htmlElement = debugElement.nativeElement;  

    expect(htmlElement.textContent).toContain('Login');
  });

  it('Should change the page title', () => {
    
    fixture.detectChanges();
    component.setTitle('title test');
    fixture.detectChanges();

    debugElement = fixture.debugElement.query(By.css('ion-title'));
    htmlElement = debugElement.nativeElement;  

    expect(htmlElement.textContent).toContain('title test');
  });

  it('Should change the page title to "Register"', () => {
    
    fixture.detectChanges();
    
    let buttonRegister = fixture.debugElement.query(By.css('#btnRegister')).nativeElement;
    buttonRegister.click();  

    fixture.detectChanges();

    debugElement = fixture.debugElement.query(By.css('ion-title'));
    htmlElement = debugElement.nativeElement;  

    expect(component.registerFlag).toBeTruthy();
    expect(htmlElement.textContent).toContain('Register');

  });

  it('Should validate login incorrectly', () => {
    
    //fixture.detectChanges();
    
    // let buttonRegister = fixture.debugElement.query(By.css('#btnLogin')).nativeElement;
    // buttonRegister.click();  

    //fixture.detectChanges();

    expect(component.isLoginValidated("jc@gmail.com","")).toBeFalsy();
    
  });




});