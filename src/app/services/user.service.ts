import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from "rxjs/operators";

//Models
import { UserModel } from '../models/user.model';
import { ResponseModel } from '../models/response.model';
import { FormGroup, FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiRoot: string = 'https://hidden-bayou-27516.herokuapp.com/api/';
  //private resultRAW: any;

  constructor(private http: HttpClient) { }

  register(user: UserModel): Observable<ResponseModel> {

    let apiRoot = this.apiRoot + 'register';
    return this.http.post(apiRoot, user).pipe(map(res => {

      let response = new ResponseModel();
      let resultRAW: any = res;

      //Set response
      response.status = resultRAW.success;
      response.message = resultRAW.message;
      response.result = resultRAW.result;
      response.records = resultRAW.result.length;

      return response;

    }),
      catchError(error => {
        return throwError(error.message);
      }));
  }

  login(user: UserModel): Observable<ResponseModel> {

    let apiRoot = this.apiRoot + 'login';

    return this.http.post(apiRoot, user).pipe(map(res => {

      let response = new ResponseModel();
      let resultRAW: any = res;

      //Set response
      response.status = resultRAW.success;
      response.message = resultRAW.message;
      response.result = resultRAW.result;
      response.records = resultRAW.result.length;

      return response;

    }),
      catchError(error => {
        return throwError(error.message);
      }));
  }
}
