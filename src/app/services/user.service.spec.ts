import { TestBed, inject, tick, fakeAsync } from '@angular/core/testing';

import { UserService } from './user.service';
//import { HttpClientModule } from '@angular/common/http';
//import { HttpTestingController } from '@angular/common/http/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

//Model
import { UserModel } from '../models/user.model';
import { ResponseModel } from '../models/response.model';

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule ]
  }));

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('Should register correctly', fakeAsync( inject([UserService], (userService) => {

    const service: UserService = TestBed.get(UserService);
    const httpMock = TestBed.get(HttpTestingController);

    const responseFake = new ResponseModel();
    responseFake.status = true;
    responseFake.message = "ok";
    responseFake.result = [];
    responseFake.records = responseFake.result.length;

    let user = new UserModel();
    user.email = "jclavotafur@gmail.com";
    user.password = "123";
    user.repassword = "123";

    let response = new ResponseModel();

    service.register(user).subscribe(
      
      (receivedResponse: ResponseModel) => {
        response = receivedResponse;
      },
      (error: any) => {}
    
    );

    const request = httpMock.expectOne('http://127.0.0.1:8000/api/genres');
    request.flush(responseFake);

    tick(); // Wait to finish subscribe call

    expect(request.request.method).toBe('POST');
    expect(response.status).toBe(true);
    expect(response).toEqual(responseFake);

  })));

  it('Should register not correctly', fakeAsync( inject([UserService], (userService) => {

    const service: UserService = TestBed.get(UserService);
    const httpMock = TestBed.get(HttpTestingController);

    const responseFake = new ResponseModel();
    responseFake.status = false;
    responseFake.message = "empty email";
    responseFake.result = [];
    responseFake.records = responseFake.result.length;

    let user = new UserModel();
    user.email = "";
    user.password = "123";
    user.repassword = "123";

    let response = new ResponseModel();

    service.register(user).subscribe(
      
      (receivedResponse: ResponseModel) => {
        response = receivedResponse;
      },
      (error: any) => {}
    
    );

    const request = httpMock.expectOne('http://127.0.0.1:8000/api/genres');
    request.flush(responseFake);

    tick(); // Wait to finish subscribe call

    expect(request.request.method).toBe('POST');
    expect(response.status).toBe(false);
    expect(response).toEqual(responseFake);

  })));

  it('Should login correctly', fakeAsync( inject([UserService], (userService) => {

    const service: UserService = TestBed.get(UserService);
    const httpMock = TestBed.get(HttpTestingController);

    const responseFake = new ResponseModel();
    responseFake.status = false;
    responseFake.message = "ok";
    responseFake.result = [1,3];
    responseFake.records = responseFake.result.length;

    let user = new UserModel();
    user.email = "";
    user.password = "123";

    let response = new ResponseModel();

    service.login(user).subscribe(
      
      (receivedResponse: ResponseModel) => {
        response = receivedResponse;
      },
      (error: any) => {}
    
    );

    const request = httpMock.expectOne('http://127.0.0.1:8000/api/genres');
    request.flush(responseFake);

    tick(); // Wait to finish subscribe call

    expect(request.request.method).toBe('POST');
    expect(response.status).toBe(false);
    expect(response).toEqual(responseFake);

  })));




});

