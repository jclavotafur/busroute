import { TestBed } from '@angular/core/testing';

import { ValidatorService } from './validator.service';

describe('ValidatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValidatorService = TestBed.get(ValidatorService);
    expect(service).toBeTruthy();
  });

  it('should validate when value is undefined', () => {
    const service: ValidatorService = TestBed.get(ValidatorService);
    let value: string;
    expect(service.stringHasData(value)).toBeFalsy();
  });

  it('should validate value is null', () => {
    const service: ValidatorService = TestBed.get(ValidatorService);
    const value: string = null;
    expect(service.stringHasData(value)).toBeFalsy();
  });

  it('should validate value is empty', () => {
    const service: ValidatorService = TestBed.get(ValidatorService);
    const value: string = "";
    expect(service.stringHasData(value)).toBeFalsy();
  });

  it('should validate value has blank spaces', () => {
    const service: ValidatorService = TestBed.get(ValidatorService);
    const value: string = " ";
    expect(service.stringHasData(value)).toBeFalsy();
  });

  it('should validate incorrect email', () => {
    const service: ValidatorService = TestBed.get(ValidatorService);

    expect(service.isEmail("jclavotafur")).toBeFalsy();
  
    expect(service.isEmail("jclavotafur@")).toBeFalsy();

    expect(service.isEmail("jclavotafur@gmail")).toBeFalsy();

    expect(service.isEmail("")).toBeFalsy();
  });

  it('should validate correct email', () => {
    const service: ValidatorService = TestBed.get(ValidatorService);
    const value: string = "jclavotafur@gmail.com";

    expect(service.isEmail(value)).toBeTruthy();
  });


  // it('xxx', () => {
  //   const service: ValidatorService = TestBed.get(ValidatorService);
  //   const value: string = "jclavotafur@gmail.com";

  //   expect(service.toBeTruthy();
  // });








});
