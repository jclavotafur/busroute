import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  constructor() { }

  stringHasData(inputValue: string): boolean
  {
    if(inputValue == undefined) return false;
    if(inputValue == null) return false;

    inputValue = inputValue.trim();
    if(inputValue == "") return false;

    return true;
  }

  stringHasDataMessage()
  {
    return "value is empty";
  }

  isEmail(email: string)
  {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
      return true
    
    return false
  }

  isEmailMessage()
  {
    return "incorrect format to email";
  }

  getCurrentTime()
  {
    var d = new Date();
    return d.getDate() + '.' + ( (d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : d.getMonth() + 1) + '.' + d.getFullYear();
    //return d.getDate() +  ( (d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : d.getMonth() + 1) + d.getFullYear();
  }

}
