import { Injectable } from '@angular/core';

import { UserModel } from '../models/user.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private isLogged = false;
  public  user = new UserModel();

  constructor(private router: Router) {}

  setLoggedIn(_value) {
    this.isLogged = _value;
  }
  isAuthenticated(): boolean {
    return this.isLogged;
  }

  setUser(_user: any)
  {
    this.user.id = _user.id;
    this.user.name = _user.name;
    this.user.email = _user.email;
    this.user.api_token = _user.api_token;
  }

  closeSession()
  {
    this.setLoggedIn(false);
    //this.router.navigateByUrl('/login', { skipLocationChange: true });
    this.router.navigate(['/login']);
  }
}
