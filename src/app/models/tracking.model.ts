export class TrackingModel {

    public id: string;
    public number: string;
    public latitude: string;
    public longitude: string;
    public date: string;
}