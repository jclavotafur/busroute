export class ResponseModel {

    public status: boolean;
    public message: string;
    public result: any;
    public records: number = 0;

}