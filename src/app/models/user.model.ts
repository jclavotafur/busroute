export class UserModel {

    public id?: string;
    public name: string;
    public email: string;
    public password: string;
    public c_password: string;
    public api_token: string;

}